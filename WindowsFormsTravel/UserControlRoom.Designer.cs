﻿namespace WindowsFormsTravel
{
    partial class UserControlRoom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Unesi = new System.Windows.Forms.Button();
            this.tableRooms = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImeApartmana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Velicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Klima = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.update = new System.Windows.Forms.DataGridViewButtonColumn();
            this.delete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableRooms)).BeginInit();
            this.SuspendLayout();
            // 
            // Unesi
            // 
            this.Unesi.BackColor = System.Drawing.Color.Silver;
            this.Unesi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Unesi.Location = new System.Drawing.Point(26, 31);
            this.Unesi.Name = "Unesi";
            this.Unesi.Size = new System.Drawing.Size(154, 26);
            this.Unesi.TabIndex = 1;
            this.Unesi.Text = "Unesi novu sobu";
            this.Unesi.UseVisualStyleBackColor = false;
            this.Unesi.Click += new System.EventHandler(this.Unesi_Click);
            // 
            // tableRooms
            // 
            this.tableRooms.AllowUserToAddRows = false;
            this.tableRooms.AllowUserToDeleteRows = false;
            this.tableRooms.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableRooms.BackgroundColor = System.Drawing.Color.White;
            this.tableRooms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Ime,
            this.ImeApartmana,
            this.Velicina,
            this.TV,
            this.Klima,
            this.update,
            this.delete});
            this.tableRooms.Location = new System.Drawing.Point(26, 75);
            this.tableRooms.Name = "tableRooms";
            this.tableRooms.ReadOnly = true;
            this.tableRooms.RowHeadersWidth = 51;
            this.tableRooms.Size = new System.Drawing.Size(602, 375);
            this.tableRooms.TabIndex = 4;
            this.tableRooms.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableRooms_CellContentClick);
            // 
            // ID
            // 
            this.ID.FillWeight = 50F;
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Ime
            // 
            this.Ime.HeaderText = "Ime";
            this.Ime.MinimumWidth = 6;
            this.Ime.Name = "Ime";
            this.Ime.ReadOnly = true;
            // 
            // ImeApartmana
            // 
            this.ImeApartmana.HeaderText = "ImeApartmana";
            this.ImeApartmana.Name = "ImeApartmana";
            this.ImeApartmana.ReadOnly = true;
            // 
            // Velicina
            // 
            this.Velicina.HeaderText = "Velicina";
            this.Velicina.MinimumWidth = 6;
            this.Velicina.Name = "Velicina";
            this.Velicina.ReadOnly = true;
            // 
            // TV
            // 
            this.TV.HeaderText = "TV";
            this.TV.MinimumWidth = 6;
            this.TV.Name = "TV";
            this.TV.ReadOnly = true;
            this.TV.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Klima
            // 
            this.Klima.HeaderText = "Klima";
            this.Klima.MinimumWidth = 6;
            this.Klima.Name = "Klima";
            this.Klima.ReadOnly = true;
            this.Klima.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Klima.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // update
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.update.DefaultCellStyle = dataGridViewCellStyle1;
            this.update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.update.HeaderText = "";
            this.update.MinimumWidth = 6;
            this.update.Name = "update";
            this.update.ReadOnly = true;
            this.update.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.update.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // delete
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.IndianRed;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.IndianRed;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.delete.DefaultCellStyle = dataGridViewCellStyle2;
            this.delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete.HeaderText = "";
            this.delete.MinimumWidth = 6;
            this.delete.Name = "delete";
            this.delete.ReadOnly = true;
            this.delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.delete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // UserControlRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableRooms);
            this.Controls.Add(this.Unesi);
            this.Name = "UserControlRoom";
            this.Size = new System.Drawing.Size(663, 485);
            ((System.ComponentModel.ISupportInitialize)(this.tableRooms)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Unesi;
        private System.Windows.Forms.DataGridView tableRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImeApartmana;
        private System.Windows.Forms.DataGridViewTextBoxColumn Velicina;
        private System.Windows.Forms.DataGridViewCheckBoxColumn TV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Klima;
        private System.Windows.Forms.DataGridViewButtonColumn update;
        private System.Windows.Forms.DataGridViewButtonColumn delete;
    }
}
