﻿namespace WindowsFormsTravel
{
    class Constants
    {
        public const string POST = "POST";
        public const string PUT = "PUT";
        public const string DELETE = "DELETE";
        public const string GET = "GET";

        public const string URL_ROOT = "https://localhost:5001/";
        public const string URL_GUESTS = "api/Guests/";
        public const string URL_ROOMS = "api/Rooms/data";
        public const string URL_ROOMS_POST = "api/Rooms/";
        public const string URL_APARTMENTS = "api/Apartments/";
        public const string URL_RESERVATIONS = "api/Reservations/data";
        public const string URL_RESERVATIONS_POST = "api/Reservations/";

    }
}