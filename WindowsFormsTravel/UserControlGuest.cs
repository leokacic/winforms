﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class UserControlGuest : UserControl
    {
        public UserControlGuest()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            InitializeComponent();
            GetGridData();
        }
        private void GetGridData()
        {
            try
            {
                JArray guests = RestHelper.GetRestData(Constants.URL_ROOT + Constants.URL_GUESTS);
                List<Guest> lista = guests.ToObject<List<Guest>>();
                tableGuest.Rows.Clear();
                foreach (var g in lista)
                {
                    tableGuest.Rows.Add(new object[] {
                        g.IdGuest,
                        g.Name,
                        g.Surname,
                        g.Address,
                        g.Age,
                        "Edit",
                        "Delete"
                    });
                }
            }
            catch (WebException webex)
            {
                Form1.error = true;
                Form1.textError = webex.Message;
            }
        }
        private void Unesi_Click(object sender, EventArgs e)
        {
            new FormAddEditGuest().ShowDialog();
            GetGridData();
        }

        private async void tableGuest_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            Guest guestForEdit = new Guest();
            DataGridViewRow row = tableGuest.Rows[index];
            guestForEdit.IdGuest = Convert.ToInt32(row.Cells["ID"].Value);
            guestForEdit.Name = Convert.ToString(row.Cells["Ime"].Value);
            guestForEdit.Surname = Convert.ToString(row.Cells["Prezime"].Value);
            guestForEdit.Address = Convert.ToString(row.Cells["Adresa"].Value);
            guestForEdit.Age = Convert.ToInt32(row.Cells["Godine"].Value);
            if (e.ColumnIndex == 5)
            {
                new FormAddEditGuest(guestForEdit).ShowDialog();
            }
            else if (e.ColumnIndex == 6)
            {
                if (MessageBox.Show("Jeste li sigurni da želite obrisati gosta: " + guestForEdit.Name + " " + guestForEdit.Surname + "?", "Obavijest", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                {
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_GUESTS + guestForEdit.IdGuest, Constants.DELETE, null);
                }
            }
            GetGridData();
        }
    }
}
