﻿namespace WindowsFormsTravel
{
    partial class UserControlReservation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Unesi = new System.Windows.Forms.Button();
            this.tableReservation = new System.Windows.Forms.DataGridView();
            this.Ime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Soba = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apartman = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumOd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumDo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableReservation)).BeginInit();
            this.SuspendLayout();
            // 
            // Unesi
            // 
            this.Unesi.BackColor = System.Drawing.Color.Silver;
            this.Unesi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Unesi.Location = new System.Drawing.Point(26, 31);
            this.Unesi.Name = "Unesi";
            this.Unesi.Size = new System.Drawing.Size(154, 26);
            this.Unesi.TabIndex = 5;
            this.Unesi.Text = "Unesi novu rezervaciju";
            this.Unesi.UseVisualStyleBackColor = false;
            this.Unesi.Click += new System.EventHandler(this.Unesi_Click);
            // 
            // tableReservation
            // 
            this.tableReservation.AllowUserToAddRows = false;
            this.tableReservation.AllowUserToDeleteRows = false;
            this.tableReservation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableReservation.BackgroundColor = System.Drawing.Color.White;
            this.tableReservation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableReservation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableReservation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ime,
            this.Prezime,
            this.Soba,
            this.Apartman,
            this.DatumOd,
            this.DatumDo});
            this.tableReservation.Location = new System.Drawing.Point(26, 75);
            this.tableReservation.Name = "tableReservation";
            this.tableReservation.RowHeadersWidth = 51;
            this.tableReservation.Size = new System.Drawing.Size(602, 375);
            this.tableReservation.TabIndex = 6;
            this.tableReservation.Tag = "guest";
            // 
            // Ime
            // 
            this.Ime.HeaderText = "Ime";
            this.Ime.MinimumWidth = 6;
            this.Ime.Name = "Ime";
            // 
            // Prezime
            // 
            this.Prezime.HeaderText = "Prezime";
            this.Prezime.MinimumWidth = 6;
            this.Prezime.Name = "Prezime";
            // 
            // Soba
            // 
            this.Soba.HeaderText = "Soba";
            this.Soba.MinimumWidth = 6;
            this.Soba.Name = "Soba";
            // 
            // Apartman
            // 
            this.Apartman.HeaderText = "Apartman";
            this.Apartman.MinimumWidth = 6;
            this.Apartman.Name = "Apartman";
            // 
            // DatumOd
            // 
            this.DatumOd.FillWeight = 150F;
            this.DatumOd.HeaderText = "Datum početka";
            this.DatumOd.MinimumWidth = 6;
            this.DatumOd.Name = "DatumOd";
            // 
            // DatumDo
            // 
            this.DatumDo.FillWeight = 115F;
            this.DatumDo.HeaderText = "Datum kraja";
            this.DatumDo.MinimumWidth = 6;
            this.DatumDo.Name = "DatumDo";
            // 
            // UserControlReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableReservation);
            this.Controls.Add(this.Unesi);
            this.Name = "UserControlReservation";
            this.Size = new System.Drawing.Size(829, 606);
            ((System.ComponentModel.ISupportInitialize)(this.tableReservation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Unesi;
        private System.Windows.Forms.DataGridView tableReservation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prezime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Soba;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apartman;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumOd;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumDo;
    }
}
