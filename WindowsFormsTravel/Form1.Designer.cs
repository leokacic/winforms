﻿namespace WindowsFormsTravel
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.userControlGuest = new WindowsFormsTravel.UserControlGuest();
            this.userControlRoom = new WindowsFormsTravel.UserControlRoom();
            this.userControlReservation = new WindowsFormsTravel.UserControlReservation();
            this.userControlApartment = new WindowsFormsTravel.UserControlApartment();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonRezervacije = new System.Windows.Forms.Button();
            this.buttonApartmani = new System.Windows.Forms.Button();
            this.buttonSobe = new System.Windows.Forms.Button();
            this.buttonGosti = new System.Windows.Forms.Button();
            this.performanceCounter1 = new System.Diagnostics.PerformanceCounter();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.userControlGuest);
            this.panel1.Controls.Add(this.userControlRoom);
            this.panel1.Controls.Add(this.userControlReservation);
            this.panel1.Controls.Add(this.userControlApartment);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1105, 519);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panel3.Controls.Add(this.labelTitle);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1104, 75);
            this.panel3.TabIndex = 1;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.Black;
            this.labelTitle.Location = new System.Drawing.Point(605, 22);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(128, 30);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "Unos gostiju";
            // 
            // userControlGuest
            // 
            this.userControlGuest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.userControlGuest.Location = new System.Drawing.Point(218, 78);
            this.userControlGuest.Margin = new System.Windows.Forms.Padding(5);
            this.userControlGuest.Name = "userControlGuest";
            this.userControlGuest.Size = new System.Drawing.Size(839, 412);
            this.userControlGuest.TabIndex = 0;
            // 
            // userControlRoom
            // 
            this.userControlRoom.Location = new System.Drawing.Point(217, 78);
            this.userControlRoom.Margin = new System.Windows.Forms.Padding(4);
            this.userControlRoom.Name = "userControlRoom";
            this.userControlRoom.Size = new System.Drawing.Size(840, 412);
            this.userControlRoom.TabIndex = 5;
            // 
            // userControlReservation
            // 
            this.userControlReservation.Location = new System.Drawing.Point(217, 78);
            this.userControlReservation.Margin = new System.Windows.Forms.Padding(4);
            this.userControlReservation.Name = "userControlReservation";
            this.userControlReservation.Size = new System.Drawing.Size(887, 412);
            this.userControlReservation.TabIndex = 6;
            // 
            // userControlApartment
            // 
            this.userControlApartment.Location = new System.Drawing.Point(217, 78);
            this.userControlApartment.Margin = new System.Windows.Forms.Padding(4);
            this.userControlApartment.Name = "userControlApartment";
            this.userControlApartment.Size = new System.Drawing.Size(840, 412);
            this.userControlApartment.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.buttonRezervacije);
            this.panel2.Controls.Add(this.buttonApartmani);
            this.panel2.Controls.Add(this.buttonSobe);
            this.panel2.Controls.Add(this.buttonGosti);
            this.panel2.Location = new System.Drawing.Point(0, 78);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(209, 441);
            this.panel2.TabIndex = 1;
            // 
            // buttonRezervacije
            // 
            this.buttonRezervacije.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonRezervacije.FlatAppearance.BorderSize = 0;
            this.buttonRezervacije.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleTurquoise;
            this.buttonRezervacije.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRezervacije.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRezervacije.Location = new System.Drawing.Point(0, 161);
            this.buttonRezervacije.Margin = new System.Windows.Forms.Padding(4);
            this.buttonRezervacije.Name = "buttonRezervacije";
            this.buttonRezervacije.Size = new System.Drawing.Size(209, 62);
            this.buttonRezervacije.TabIndex = 3;
            this.buttonRezervacije.Text = "Rezervacije";
            this.buttonRezervacije.UseVisualStyleBackColor = false;
            this.buttonRezervacije.Click += new System.EventHandler(this.buttonRezervacije_Click);
            // 
            // buttonApartmani
            // 
            this.buttonApartmani.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonApartmani.FlatAppearance.BorderSize = 0;
            this.buttonApartmani.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleTurquoise;
            this.buttonApartmani.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonApartmani.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonApartmani.Location = new System.Drawing.Point(0, 55);
            this.buttonApartmani.Margin = new System.Windows.Forms.Padding(4);
            this.buttonApartmani.Name = "buttonApartmani";
            this.buttonApartmani.Size = new System.Drawing.Size(209, 55);
            this.buttonApartmani.TabIndex = 2;
            this.buttonApartmani.Text = "Apartmani";
            this.buttonApartmani.UseVisualStyleBackColor = false;
            this.buttonApartmani.Click += new System.EventHandler(this.ButtonApartmani_Click);
            // 
            // buttonSobe
            // 
            this.buttonSobe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonSobe.FlatAppearance.BorderSize = 0;
            this.buttonSobe.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleTurquoise;
            this.buttonSobe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSobe.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSobe.Location = new System.Drawing.Point(0, 103);
            this.buttonSobe.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSobe.Name = "buttonSobe";
            this.buttonSobe.Size = new System.Drawing.Size(209, 62);
            this.buttonSobe.TabIndex = 1;
            this.buttonSobe.Text = "Sobe";
            this.buttonSobe.UseVisualStyleBackColor = false;
            this.buttonSobe.Click += new System.EventHandler(this.ButtonSobe_Click);
            // 
            // buttonGosti
            // 
            this.buttonGosti.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonGosti.FlatAppearance.BorderSize = 0;
            this.buttonGosti.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleTurquoise;
            this.buttonGosti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGosti.Font = new System.Drawing.Font("Segoe UI", 15.75F);
            this.buttonGosti.Location = new System.Drawing.Point(0, 0);
            this.buttonGosti.Margin = new System.Windows.Forms.Padding(4);
            this.buttonGosti.Name = "buttonGosti";
            this.buttonGosti.Size = new System.Drawing.Size(209, 58);
            this.buttonGosti.TabIndex = 0;
            this.buttonGosti.Text = "Gosti";
            this.buttonGosti.UseVisualStyleBackColor = false;
            this.buttonGosti.Click += new System.EventHandler(this.ButtonGosti_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 520);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonApartmani;
        private System.Windows.Forms.Button buttonSobe;
        private System.Windows.Forms.Button buttonGosti;
        private UserControlGuest userControlGuest;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonRezervacije;
        private System.Windows.Forms.Label labelTitle;
        private System.Diagnostics.PerformanceCounter performanceCounter1;
        private UserControlApartment userControlApartment;
        private UserControlRoom userControlRoom;
        private UserControlReservation userControlReservation;
    }
}

