﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsTravel
{

    public partial class FormAddEditRoom : Form
    {
        Room _Room = null;
        public FormAddEditRoom()
        {
            InitializeComponent();
            label1.Text = "Unesi novu sobu";
            GetComboBoxApartments();
        }

        public FormAddEditRoom(Room room)
        {

            InitializeComponent();
            GetComboBoxApartments();
            Accept.Text = "Update";
            _Room = room;
            label1.Text = "Ažuriraj sobu";
            txtName.Text = _Room.Name;
            txtSize.Text = _Room.Size.ToString();
            checkBoxTv.Checked = _Room.Tv;
            checkBoxAirConditioner.Checked = _Room.AirConditioner;
            int index = comboBoxApartments.FindString(_Room.ApartmentName);
            comboBoxApartments.SelectedIndex = index;
        }

        private async void Accept_Click(object sender, EventArgs e)
        {
            if (_Room == null)
            {
                //new
                Room room = new Room();
                room.Name = txtName.Text;
                
                room.Tv = checkBoxTv.Checked;
                room.AirConditioner = checkBoxAirConditioner.Checked;
                room.IdApartment = Int32.Parse(((KeyValuePair<string, string>)comboBoxApartments.SelectedItem).Key);
                var isNumeric = int.TryParse(txtSize.Text, out int n);
                if (!isNumeric)
                    showError("Neispravan format podataka za veličinu sobu. Molimo unesite broj!");
                else
                {
                    room.Size = Int32.Parse(txtSize.Text);
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_ROOMS_POST, Constants.POST, room);
                    this.Hide();
                }
            }
            else
            {
                //update
                _Room.Name = txtName.Text;
                
                _Room.Tv = checkBoxTv.Checked;
                _Room.AirConditioner = checkBoxAirConditioner.Checked;
                _Room.IdApartment = Int32.Parse(((KeyValuePair<string, string>)comboBoxApartments.SelectedItem).Key);
                var isNumericUpdate = int.TryParse(txtSize.Text, out int n);
                if (!isNumericUpdate)
                    showError("Neispravan format podataka za veličinu sobu. Molimo unesite broj!");
                else
                {
                    _Room.Size = Int32.Parse(txtSize.Text);
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_ROOMS_POST + _Room.IdRoom, Constants.PUT, _Room);
                    this.Hide();
                }
            }

        }

        private void GetComboBoxApartments()
        {
            try
            {
                JArray apartments = RestHelper.GetRestData(Constants.URL_ROOT + Constants.URL_APARTMENTS);

                List<Apartment> apartmentsList = apartments.ToObject<List<Apartment>>();
                Dictionary<string, string> guestKeyPair = new Dictionary<string, string>();
                comboBoxApartments.Items.Clear();
                foreach (var apartment in apartmentsList)
                {
                    guestKeyPair.Add(apartment.IdApartment.ToString(), apartment.Name);
                }
                comboBoxApartments.DataSource = new BindingSource(guestKeyPair, null);
                comboBoxApartments.DisplayMember = "Value";
                comboBoxApartments.ValueMember = "Key";
                // string value = ((KeyValuePair<string, string>)comboBoxApartments.SelectedItem).Value;

            }
            catch (WebException webex)
            {
                MessageBox.Show("Nije ostvarena konekcija na bazu", webex.Message);
            }
        }

        private void showError(String error)
        {
            lblError.Text = error;
            lblError.Visible = true;
            timerError.Start();
        }
        private void timerError_Tick(object sender, EventArgs e)
        {
            timerError.Stop();
            lblError.Visible = false;
        }
    }
}
