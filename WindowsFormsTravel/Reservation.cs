﻿using System;

namespace WindowsFormsTravel
{
    public class Reservation
    {
        public int IdGuest { get; set; }
        public int IdRoom { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}