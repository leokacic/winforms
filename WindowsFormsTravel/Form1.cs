﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class Form1 : Form
    {
        public static Boolean error = false;
        public static String textError = null;
        public Form1()
        {
            InitializeComponent();
            if (error)
                MessageBox.Show("Nije ostvarena konekcija na bazu", textError);
        }

        private void Form1_Load(object sender, PaintEventArgs e)
        {
            userControlGuest.Show();
            userControlGuest.BringToFront();
        }

        private void ButtonGosti_Click(object sender, EventArgs e)
        {
            userControlReservation.Hide();
            userControlApartment.Hide();
            userControlRoom.Hide();

            userControlGuest.Show();
            userControlGuest.BringToFront();

            labelTitle.Text = "Unos gostiju";

            SelectedButtonBackColor(buttonGosti);
            ResetButtonBackColor(buttonRezervacije, buttonApartmani, buttonSobe);
        }

        private void ButtonSobe_Click(object sender, EventArgs e)
        {
            userControlReservation.Hide();
            userControlApartment.Hide();
            userControlGuest.Hide();

            userControlRoom.Show();
            userControlRoom.BringToFront();

            labelTitle.Text = "Unos soba";

            SelectedButtonBackColor(buttonSobe);
            ResetButtonBackColor(buttonRezervacije, buttonApartmani, buttonGosti);
        }

        private void ButtonApartmani_Click(object sender, EventArgs e)
        {
            userControlReservation.Hide();
            userControlGuest.Hide();
            userControlRoom.Hide();

            userControlApartment.Show();
            userControlApartment.BringToFront();
            labelTitle.Text = "Unos apartmana";

            SelectedButtonBackColor(buttonApartmani);
            ResetButtonBackColor(buttonRezervacije, buttonSobe, buttonGosti);
        }

        private void buttonRezervacije_Click(object sender, EventArgs e)
        {
            userControlApartment.Hide();
            userControlGuest.Hide();
            userControlRoom.Hide();

            userControlReservation.Show();
            userControlReservation.BringToFront();
            labelTitle.Text = "Unos rezervacija";

            SelectedButtonBackColor(buttonRezervacije);
            ResetButtonBackColor(buttonApartmani, buttonSobe, buttonGosti);
        }
        private void ResetButtonBackColor(Button button1, Button button2, Button button3)
        {
            button1.BackColor = Color.FromArgb(0, 192, 192);
            button2.BackColor = Color.FromArgb(0, 192, 192);
            button3.BackColor = Color.FromArgb(0, 192, 192);
        }
        private void SelectedButtonBackColor(Button button)
        {
            button.BackColor = Color.PaleTurquoise;
        }
    }
}
