﻿namespace WindowsFormsTravel
{
    partial class UserControlApartment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Unesi = new System.Windows.Forms.Button();
            this.tableApartment = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Velicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrojSoba = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parking = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.update = new System.Windows.Forms.DataGridViewButtonColumn();
            this.delete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableApartment)).BeginInit();
            this.SuspendLayout();
            // 
            // Unesi
            // 
            this.Unesi.BackColor = System.Drawing.Color.Silver;
            this.Unesi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Unesi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Unesi.Location = new System.Drawing.Point(26, 31);
            this.Unesi.Name = "Unesi";
            this.Unesi.Size = new System.Drawing.Size(154, 26);
            this.Unesi.TabIndex = 1;
            this.Unesi.Text = "Unesi novi apartman";
            this.Unesi.UseVisualStyleBackColor = false;
            this.Unesi.Click += new System.EventHandler(this.Unesi_Click);
            // 
            // tableApartment
            // 
            this.tableApartment.AllowUserToAddRows = false;
            this.tableApartment.AllowUserToDeleteRows = false;
            this.tableApartment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableApartment.BackgroundColor = System.Drawing.Color.White;
            this.tableApartment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableApartment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableApartment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Ime,
            this.Velicina,
            this.BrojSoba,
            this.Parking,
            this.update,
            this.delete});
            this.tableApartment.Location = new System.Drawing.Point(26, 75);
            this.tableApartment.Name = "tableApartment";
            this.tableApartment.ReadOnly = true;
            this.tableApartment.RowHeadersWidth = 51;
            this.tableApartment.Size = new System.Drawing.Size(602, 375);
            this.tableApartment.TabIndex = 4;
            this.tableApartment.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableApartment_CellClick);
            // 
            // ID
            // 
            this.ID.FillWeight = 50F;
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Ime
            // 
            this.Ime.HeaderText = "Ime";
            this.Ime.MinimumWidth = 6;
            this.Ime.Name = "Ime";
            this.Ime.ReadOnly = true;
            // 
            // Velicina
            // 
            this.Velicina.HeaderText = "Velicina";
            this.Velicina.Name = "Velicina";
            this.Velicina.ReadOnly = true;
            // 
            // BrojSoba
            // 
            this.BrojSoba.HeaderText = "BrojSoba";
            this.BrojSoba.Name = "BrojSoba";
            this.BrojSoba.ReadOnly = true;
            // 
            // Parking
            // 
            this.Parking.HeaderText = "Parking";
            this.Parking.Name = "Parking";
            this.Parking.ReadOnly = true;
            this.Parking.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Parking.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // update
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.update.DefaultCellStyle = dataGridViewCellStyle3;
            this.update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.update.HeaderText = "";
            this.update.Name = "update";
            this.update.ReadOnly = true;
            // 
            // delete
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.IndianRed;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.IndianRed;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.delete.DefaultCellStyle = dataGridViewCellStyle4;
            this.delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete.HeaderText = "";
            this.delete.MinimumWidth = 6;
            this.delete.Name = "delete";
            this.delete.ReadOnly = true;
            this.delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.delete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // UserControlApartment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableApartment);
            this.Controls.Add(this.Unesi);
            this.Name = "UserControlApartment";
            this.Size = new System.Drawing.Size(663, 485);
            ((System.ComponentModel.ISupportInitialize)(this.tableApartment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Unesi;
        private System.Windows.Forms.DataGridView tableApartment;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Velicina;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojSoba;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Parking;
        private System.Windows.Forms.DataGridViewButtonColumn update;
        private System.Windows.Forms.DataGridViewButtonColumn delete;
    }
}
