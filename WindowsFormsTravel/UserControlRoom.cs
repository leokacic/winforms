﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class UserControlRoom : UserControl
    {
        public UserControlRoom()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            InitializeComponent();
            GetGridData();
        }

        private void GetGridData()
        {
            try
            {
                JArray rooms = RestHelper.GetRestData(Constants.URL_ROOT + Constants.URL_ROOMS);
                List<Room> lista = rooms.ToObject<List<Room>>();

                tableRooms.Rows.Clear();
                foreach (var room in lista)
                {
                    tableRooms.Rows.Add(new object[] {
                        room.IdRoom,
                        room.Name,
                        room.ApartmentName,
                        room.Size,
                        room.Tv,
                        room.AirConditioner,                       
                        "Edit",
                        "Delete"
                    });
                }
            }
            catch (WebException webex)
            {
                Form1.error = true;
                Form1.textError = webex.Message;
            }
        }

        private void Unesi_Click(object sender, EventArgs e)
        {
            new FormAddEditRoom().ShowDialog();
            GetGridData();
        }

        private async void tableRooms_CellContentClick(object sender, DataGridViewCellEventArgs e)
        { 
            int index = e.RowIndex;
            Room roomForEdit = new Room();
            DataGridViewRow row = tableRooms.Rows[index];
            roomForEdit.IdRoom = Convert.ToInt32(row.Cells["ID"].Value);
            roomForEdit.Name = Convert.ToString(row.Cells["Ime"].Value);
            roomForEdit.ApartmentName = Convert.ToString(row.Cells["ImeApartmana"].Value);
            roomForEdit.Size = Convert.ToInt32(row.Cells["Velicina"].Value);
            roomForEdit.Tv = Convert.ToBoolean(row.Cells["TV"].Value);
            roomForEdit.AirConditioner = Convert.ToBoolean(row.Cells["Klima"].Value);
            if (e.ColumnIndex == 6)
            {
                new FormAddEditRoom(roomForEdit).ShowDialog();
            }
            else if (e.ColumnIndex == 7)
            {
                if (MessageBox.Show("Jeste li sigurni da želite obrisati sobu: " + roomForEdit.Name + "?", "Obavijest", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                {
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_ROOMS_POST + roomForEdit.IdRoom, Constants.DELETE, null);
                }
            }
            GetGridData();
        }
    }
}
