﻿namespace WindowsFormsTravel
{
    public class ComboBoxApartment
    {
        public string Name;
        public int Value;
        public ComboBoxApartment(string Name, int Value)
        {
            this.Name = Name;
            this.Value = Value;
        }
    }
}
