﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsTravel
{
    class RestHelper
    {
        public static JArray GetRestData(string uri)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(uri);
            var webResponse = (HttpWebResponse)webRequest.GetResponse();
            var reader = new StreamReader(webResponse.GetResponseStream());
            string s = reader.ReadToEnd();
            return JsonConvert.DeserializeObject<JArray>(s);
        }
        public static async Task<string> CallWebAPi(String url, String request, Object data)
        {
            string response = string.Empty;
            var json = "";

            if (request.CompareTo(Constants.POST) == 0 || request.CompareTo(Constants.PUT) == 0)
            {
                json = JsonConvert.SerializeObject(data);
            }

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            if (request.CompareTo(Constants.POST) == 0)
            {
                HttpResponseMessage res = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                response = await getResponse(res);
            }
            else if (request.CompareTo(Constants.GET) == 0)
            {
                HttpResponseMessage res = await client.GetAsync(url);
                response = await getResponse(res);
            }
            else if (request.CompareTo(Constants.PUT) == 0)
            {
                HttpResponseMessage res = await client.PutAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                response = await getResponse(res);
            }
            else if (request.CompareTo(Constants.DELETE) == 0)
            {
                HttpResponseMessage res = await client.DeleteAsync(url);
                Debug.WriteLine(url);
                response = await getResponse(res);
            }
            return response;
        }

        public static async Task<string> getResponse(HttpResponseMessage res)
        {
            using (HttpContent content = res.Content)
            {
                string response = await content.ReadAsStringAsync();
                if (response != null)
                {
                    return response;
                }
            }
            return string.Empty;
        }
    }
}
