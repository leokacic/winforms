﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class FormAddReservation : Form
    {
        ReservationData _ReservationData = null;
        DateTime startDate;
        DateTime endDate;

        public FormAddReservation()
        {
            InitializeComponent();
            label1.Text = "Unesi novu rezervaciju";
            GetComboBoxGuests();
            GetComboBoxRooms();
        }

        private void GetComboBoxRooms()
        {
            try
            {
                JArray rooms = RestHelper.GetRestData(Constants.URL_ROOT + Constants.URL_ROOMS);

                List<Room> roomList = rooms.ToObject<List<Room>>();
                Dictionary<string, string> roomKeyPair = new Dictionary<string, string>();
                comboBoxRoom.Items.Clear();
                foreach (var room in roomList)
                {
                    roomKeyPair.Add(room.IdRoom.ToString(), room.Name);
                }
                comboBoxRoom.DataSource = new BindingSource(roomKeyPair, null);
                comboBoxRoom.DisplayMember = "Value";
                comboBoxRoom.ValueMember = "Key";
                string value = ((KeyValuePair<string, string>)comboBoxRoom.SelectedItem).Value;

            }
            catch (WebException webex)
            {
                MessageBox.Show("Nije ostvarena konekcija na bazu", webex.Message);
            }
        }

        private void GetComboBoxGuests()
        {
            try
            {
                JArray guests = RestHelper.GetRestData(Constants.URL_ROOT + Constants.URL_GUESTS);

                List<Guest> guestList = guests.ToObject<List<Guest>>();
                Dictionary<string, string> guestKeyPair = new Dictionary<string, string>();
                comboBoxGuest.Items.Clear();
                foreach (var guest in guestList)
                {
                    guestKeyPair.Add(guest.IdGuest.ToString(), guest.Name + " " + guest.Surname);
                }
                comboBoxGuest.DataSource = new BindingSource(guestKeyPair, null);
                comboBoxGuest.DisplayMember = "Value";
                comboBoxGuest.ValueMember = "Key";
                string value = ((KeyValuePair<string, string>)comboBoxGuest.SelectedItem).Value;

            }
            catch (WebException webex)
            {
                MessageBox.Show("Nije ostvarena konekcija na bazu", webex.Message);
            }
        }

        private void monthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            startDate = e.Start;
            endDate = e.End;
        }

        private void monthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            startDate = e.Start;
            endDate = e.End;
        }

        private async void buttonSave_Click(object sender, EventArgs e)
        {
            //new
            Reservation reservation = new Reservation();
            reservation.IdGuest = Int32.Parse(((KeyValuePair<string, string>)comboBoxGuest.SelectedItem).Key);
            reservation.IdRoom = Int32.Parse(((KeyValuePair<string, string>)comboBoxRoom.SelectedItem).Key);
            reservation.StartDate = startDate;
            reservation.EndDate = endDate;
            var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_RESERVATIONS_POST, Constants.POST, reservation);
            this.Hide();
        }
    }
}
