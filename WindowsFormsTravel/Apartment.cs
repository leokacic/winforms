﻿using System;

namespace WindowsFormsTravel
{
    public class Apartment
    {
        public int IdApartment { get; set; }
        public string Name { get; set; }
        public decimal? Size { get; set; }
        public int? Rooms { get; set; }
        public Boolean Parking { get; set; }
    }
}