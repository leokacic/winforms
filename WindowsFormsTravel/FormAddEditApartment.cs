﻿using System;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class FormAddEditApartment : Form
    {
        Apartment _Apartment = null;
        public FormAddEditApartment()
        {
            InitializeComponent();
            label1.Text = "Unesi novi apartman";
        }

        public FormAddEditApartment(Apartment apartment)
        {
            InitializeComponent();
            Accept.Text = "Update";
            _Apartment = apartment;
            label1.Text = "Ažuriraj apartman";
            txtName.Text = _Apartment.Name;
            txtSize.Text = _Apartment.Size.ToString();
            txtRooms.Text = _Apartment.Rooms.ToString();
            checkBoxParking.Checked = _Apartment.Parking;
        }

        private async void Accept_Click(object sender, EventArgs e)
        {
            if (_Apartment == null)
            {
                //new
                Apartment apartment = new Apartment();
                apartment.Name = txtName.Text;
                apartment.Parking = checkBoxParking.Checked;
                var isNumeric = int.TryParse(txtRooms.Text, out int n);
                var isNumeric2 = Decimal.TryParse(txtSize.Text, out Decimal a);
                if (!isNumeric || !isNumeric2)
                    showError("Neispravan format podataka za veličinu sobe ili broj soba. Molimo unesite broj!");
                else
                {
                    apartment.Rooms = Int32.Parse(txtRooms.Text);
                    apartment.Size = Decimal.Parse(txtSize.Text);
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_APARTMENTS, Constants.POST, apartment);
                    this.Hide();
                }
            }
            else
            {
                //update
                _Apartment.Name = txtName.Text;
                _Apartment.Parking = checkBoxParking.Checked;
                var isNumericUpdate = int.TryParse(txtRooms.Text, out int n);
                var isNumericUpdate2 = Decimal.TryParse(txtSize.Text, out Decimal a);
                if (!isNumericUpdate || !isNumericUpdate2)
                    showError("Neispravan format podataka za veličinu sobu ili broj soba. Molimo unesite broj!");
                else
                {
                    _Apartment.Size = Decimal.Parse(txtSize.Text);
                    _Apartment.Rooms = Int32.Parse(txtRooms.Text);
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_APARTMENTS + _Apartment.IdApartment, Constants.PUT, _Apartment);
                    this.Hide();
                }
            }
        }

        private void showError(String error)
        {
            lblError.Text = error;
            lblError.Visible = true;
            timerError.Start();
        }
        private void timerError_Tick(object sender, EventArgs e)
        {
            timerError.Stop();
            lblError.Visible = false;
        }
    }
}
