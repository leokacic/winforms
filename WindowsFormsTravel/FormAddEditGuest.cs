﻿using System;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class FormAddEditGuest : Form
    {
        Guest _Guest = null;
        public FormAddEditGuest()
        {
            InitializeComponent();
            label1.Text = "Unesi novog gosta";
        }

        public FormAddEditGuest(Guest guest)
        {
            InitializeComponent();
            button1.Text = "Update";
            _Guest = guest;
            label1.Text = "Ažuriraj gosta";
            txtName.Text = _Guest.Name;
            txtSurname.Text = _Guest.Surname;
            textAddress.Text = _Guest.Address;
            txtAge.Text = _Guest.Age.ToString();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (_Guest == null)
            {
                //new
                Guest guest = new Guest();
                guest.Name = txtName.Text;
                guest.Surname = txtSurname.Text;
                guest.Address = textAddress.Text;
                var isNumeric = int.TryParse(txtAge.Text, out int n);
                if (!isNumeric)
                    showError("Neispravan format podataka za broj godina. Molimo unesite broj!");
                else
                {
                    guest.Age = Int32.Parse(txtAge.Text);
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_GUESTS, Constants.POST, guest);
                    this.Hide();
                }
            }
            else
            {
                //update
                _Guest.Name = txtName.Text;
                _Guest.Surname = txtSurname.Text;
                _Guest.Address = textAddress.Text;
                var isNumericUpdate = int.TryParse(txtAge.Text, out int n);
                if (!isNumericUpdate)
                    showError("Neispravan format podataka za broj godina. Molimo unesite broj!");
                else
                {
                    _Guest.Age = Int32.Parse(txtAge.Text);
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_GUESTS + _Guest.IdGuest, Constants.PUT, _Guest);
                    this.Hide();
                }
            }
        }
        private void showError(String error)
        {
            lblError.Text = error;
            lblError.Visible = true;
            timerError.Start();
        }
        private void timerError_Tick(object sender, EventArgs e)
        {
            timerError.Stop();
            lblError.Visible = false;
        }
    }
}
