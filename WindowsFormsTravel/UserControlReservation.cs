﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class UserControlReservation : UserControl
    {
        public UserControlReservation()
        {
            InitializeComponent();
            GetGridData();
        }

        private void Unesi_Click(object sender, EventArgs e)
        {
            new FormAddReservation().ShowDialog();
            GetGridData();
        }

        private void GetGridData()
        {
            try
            {
                JArray reservations = RestHelper.GetRestData(Constants.URL_ROOT + Constants.URL_RESERVATIONS);
                List<ReservationData> resList = reservations.ToObject<List<ReservationData>>();
                tableReservation.Rows.Clear();
                foreach (var res in resList)
                {
                    tableReservation.Rows.Add(new object[] {
                       res.guestName,
                       res.guestSurname,
                       res.roomName,
                       res.apartmentName,
                       res.StartDate.ToString("dd.MM.yyyy"),
                       res.EndDate.Value.ToString("dd.MM.yyyy")
                    });
                }
            }
            catch (WebException webex)
            {
                Form1.error = true;
                Form1.textError = webex.Message;
            }
        }
    }
}