﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace WindowsFormsTravel
{
    public partial class UserControlApartment : UserControl
    {
        public UserControlApartment()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            InitializeComponent();
            GetGridData();
        }

        private JArray GetRESTData(string uri)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(uri);
            var webResponse = (HttpWebResponse)webRequest.GetResponse();
            var reader = new StreamReader(webResponse.GetResponseStream());
            string s = reader.ReadToEnd();
            return JsonConvert.DeserializeObject<JArray>(s);
        }

        private void GetGridData()
        {
            try
            {
                JArray apartments = GetRESTData(Constants.URL_ROOT + Constants.URL_APARTMENTS);
                List<Apartment> lista = apartments.ToObject<List<Apartment>>();
                tableApartment.Rows.Clear();
                foreach (var apt in lista)
                {
                    tableApartment.Rows.Add(new object[] {
                        apt.IdApartment,
                        apt.Name,
                        apt.Size,
                        apt.Rooms,
                        apt.Parking,
                        "Edit",
                        "Delete"
                    });
                }
            }
            catch (WebException webex)
            {
                Form1.error = true;
                Form1.textError = webex.Message;
            }
        }

        private void Unesi_Click(object sender, EventArgs e)
        {
            new FormAddEditApartment().ShowDialog();
            GetGridData();
        }

        private async void tableApartment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            Apartment apartmentForEdit = new Apartment();
            DataGridViewRow row = tableApartment.Rows[index];
            apartmentForEdit.IdApartment = Convert.ToInt32(row.Cells["ID"].Value);
            apartmentForEdit.Name = Convert.ToString(row.Cells["Ime"].Value);
            apartmentForEdit.Size = Convert.ToDecimal(row.Cells["Velicina"].Value);
            apartmentForEdit.Rooms = Convert.ToInt32(row.Cells["BrojSoba"].Value);
            apartmentForEdit.Parking = Convert.ToBoolean(row.Cells["Parking"].Value);
            if (e.ColumnIndex == 5)
            {
                new FormAddEditApartment(apartmentForEdit).ShowDialog();
            }
            else if (e.ColumnIndex == 6)
            {
                if (MessageBox.Show("Jeste li sigurni da želite obrisati apartman: " + apartmentForEdit.Name + "?", "Obavijest", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                {
                    var response = await RestHelper.CallWebAPi(Constants.URL_ROOT + Constants.URL_APARTMENTS + apartmentForEdit.IdApartment, Constants.DELETE, null);
                }
            }
            GetGridData();
        }
    }
}
